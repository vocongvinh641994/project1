import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main()=>runApp(MaterialApp(
  debugShowCheckedModeBanner: false,
  home: MyHome(),
));

class MyHome extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => MyHomeState();
}

class MyHomeState extends State<MyHome>{
  var nameOfApp = "Persist Key Value";

  var counter =0;

  var key = "counter";

  @override
  void initState() {
    // TODO: implement initState
    _loadSavedData();
    super.initState();
  }

  _loadSavedData() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      counter = (prefs.getInt(key)?? 0);
    });
  }

  _onIncrementHit() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      counter = (prefs.getInt(key)??0)+1;
    });

    prefs.setInt(key, counter);
  }

  _onDecrementHit() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      counter = (prefs.getInt(key)?? 0)-1;
    });

    prefs.setInt(key, counter);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(nameOfApp),
      ),
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                '$counter',
                textScaleFactor: 10.0,
              ),
              Padding(padding: EdgeInsets.all(10.0),),
              RaisedButton(
                  onPressed: _onIncrementHit,
              child: Text('Increment Counter'),),
              Padding(
                  padding: EdgeInsets.all(10.0)),
              RaisedButton(
                  onPressed: _onDecrementHit,
              child: Text('Decrement Counter'),),
            ],
          ),
        ),
      ),
    );
  }
}